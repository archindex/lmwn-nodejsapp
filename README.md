## Steps to Build example-nodejs Application Using Docker-Compose and Kubernetes 

## diagram for application

<img src="img/diagram.png" width="500"/>

# __Table of Content__

- [Test](#test)
- [Build](#build)
- [Run and Deploy](#Run-and-Deploy)
- [CICD](#cicd)
- [Kubernetes](#kubernetes)
- [File and Folder](#file-and-folder)

# __Test__

## _Test App01_

After run docker-compose command ; to test app01 use browser to brows __path "/hello"__ and correct response should be as image below _(in this images using localhost as a test server)_

![](img/app01-browser.png)

or can be check in docker-compose logs that nginx container is accept connection and reply http 200 as a correct response also logs in app01 show that already connect to redis  as image below


<img src="img/app01-logs.png" width="500"/>


## _Test App02_

After run docker-compose command ; to test app02 use browser to brows __path "/hello2"__ and correct response should be as image below _(in this images using localhost as a test server)_

![](img/app02-browser.png)

or can be check in docker-compose logs that nginx container is accept connection and reply http 200 as a correct response 

<img src="img/app02-logs.png" width="700"/>

# __Build__

## __Strategy__

Build application by divide hello-1.js and hello-2.js into two folder 
- Folder app01 for hello-1.js app 
- Folder app02 for hello-2.js app

Then create dockerfile (using node:alpine as base image) for build container and build app seperately 

## _app01_
To build app01 cotainer image, use command
```sh
docker build -t lmwn-app01:$TAG app01/
```
_$TAG_ is value for tag of container images in app01 

for more information about app01 docker image please read [app01-readme](/app01/README.md) 



## _app02_
app02 build using command
```
docker build -t lmwn-app02:$TAG app02/
```
_$TAG_ is value for tag of container images in app02

for more information about app02 docker image please read [app02-readme](/app02/README.md) 

# __Run and Deploy__

### Deploy Applicaiton

For Dev Environment, please use command to define tag value as git commit hash 
```sh
export TAG=$(git log -1 --format=%h)
```

For Pros Environment, please use command to define tag value as git tag
```sh
export TAG=$(git tag --points-at HEAD)
```

Deploy application as given diagram, use command 
```sh
docker-compose up -d --scale app01=2 --scale app02=2 
```

### Deploy application using jenkins (must create git repository for this application)

- create pipeline jobs in jenkins
- install and config plugin for jenkins _"publish over ssh"_
- there are 4 steps of this pipeline 
  1. building app01 container image
  2. building app02 container image
  3. login to dockerhub and push docker images
  4. deploy application to docker-compose server //in this step using jenkins plugin "publish over ssh" to access to server and deploy application
# __CICD__ 

## __Strategy__
For applying this application to support multi environment the Jenkins file is devided into 2 file for 2 jenkins jobs base on Environment 
- Dev (git hash container images)
- Prod  (git tag container images)

<img src="img/docker-images.png" width="500"/>

### Dev Environment
As for Dev environment a pipeline job in jenkins must include the "jenkins" file and container images is building related to git commit hash and deploy to docker compose server 

### Prod Environment
As for Prod Environment, a pipeline jobs in jenkins __MUST__ use "prod/jenkinsfile" file as a pipeline script 

contianer images is building related to __git tag__ and deploy to docker compose server 

## PS. __MUST do GIT TAG__ in Prod environmnet. If there is no __GIT TAG__, the pipeline build will failed because there is no container images tag to build 

# __Kubernetes__

for kubernetes manifest file, using [KOMPOSE](https://kompose.io/) for baseline converting from docker-compose.yaml in to kubernetes manifest then change some config as detail below 

## Deployment
- app01-deployment.yaml  >> deployment for app01 (change replica to 2)
- app02-deployment.yaml  >> deployment for app02 (change replica to 2)
- redis-deployment.yaml  >> deployment for redis

## Service

- app01-service.yaml >> service for app01 
- app02-service.yaml >> service for app02
- redis-service.yaml >> service for redis

## Ingress 
delete _nginx-deployment.yaml and nginx-service.yaml_ since using ingress rule instead 

- app01-ingress.yaml >> __[add]__ ingress rule for app01 (need to uss url rewrite to apply hello to call app01 hello1)
- app02-ingress.yaml >> __[add]__ ingress rule for app02

## Deploy into kubernetes 
For Dev Environment, please use command to define tag value as git commit hash 
```sh
export TAG=$(git log -1 --format=%h)
```

For Pros Environment, please use command to define tag value as git tag
```sh
export TAG=$(git tag --points-at HEAD)
```

To deploy app into kubernetes cluster, please use commnad
```sh
cd /kubernetes
sed -i ""  "s/TAG/$TAG/g" app0*-deployment.yaml ##To assign TAG value to container images in manifest file 
kubectl apply -f . 
```

  # __File and Folder__
this section define a file and folder in this application 

- /app
    - Original Folder for hello-1.js and hello-2.js file 
- /app01
    - Folder for app01 including dockerfile and hello-1.js and dependencies 
- /app02
    - Folder for app02 including dockerfile and hello-2.js and dependencies
- /img
    - Folder for Image using in this README file 
- /kubernetes
    - Folder for keep kubernetes manifest file for app01 and app02 convert from docker-compose.yaml
- /prod
    - Folder for jenkins file which provide CI/CD pipeline for Jenkins using in prod [ONLY PROD]
- default.conf
    - nginx configuration file for nginx container in docker-compose
- docker-compose.yaml
    - docker-compose manifest file
- jenkinsfile
    - CI/CD pipeline for Jenkins using dev environment in this example application [ONLY DEV]
- README-qustion
    - Original README file 
- README
    - This README file