# app01 docker images 

## in this example using $TAG value = 1.0.0

Build docker images command for app01 application
```sh
docker build -t lmwn-app01:1.0.0 .
```

testing app01 by command docker run 
```sh
docker run -it -d  --name app01 -p 80:8000 lmwn-app01:1.0.0
```